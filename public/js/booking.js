window.onload = function () {
    let check = document.getElementById('check');
    let upgraded = document.getElementById('upgraded');
    upgraded.style.display = 'none';

    check.addEventListener('click', on_click);
};

function on_click()
{
    let vehicle = document.getElementById('vehicle');
    let vehicle_id = vehicle.selectedOptions[0].value;

    let client = document.getElementById('client');
    let client_id = client.selectedOptions[0].value;

    let httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = state_change;
    let url = '/api/clients/' + client_id + '/upgradable';
    httpRequest.open("POST", url);
    httpRequest.setRequestHeader(
        "Content-Type",
        "application/x-www-form-urlencoded"
    );
    httpRequest.send("vehicle_id=" + encodeURIComponent(vehicle_id));
}

function state_change()
{
    let client = document.getElementById('client');
    let name = client.selectedOptions[0].label;
    let upgraded = document.getElementById('upgraded');
    let message = document.getElementById('message');

    if (this.readyState === XMLHttpRequest.DONE) {
        if (this.status === 200) {
            let data = JSON.parse(this.responseText);

            if (data.upgradable) {
                upgraded.style.display = 'inline';
                message.className = 'alert alert-success';
                message.textContent = name + ' a le droit d’être surclassé sur les TieFighters';
                return;
            }
        }
        else {
            upgraded.style.display = 'inline';
            message.className = 'alert alert-danger';
            message.textContent = 'Nous avons un soucis temporaire avec le back-office. Libre à vous de surclasser ' + name;
            return;
        }
    }

    upgraded.style.display = 'none';
    message.className = 'alert alert-warning';
    message.textContent = name + ' ne peut être surclassé sur les TieFighters et doit donc rester sur les XWing';
}
