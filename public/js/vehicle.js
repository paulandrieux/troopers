window.onload = function() {
    let type = document.getElementById('type');

    type.addEventListener('change', function (event) {
        toggle_color(event.target);
    });

    var event = new Event('change');
    type.dispatchEvent(event);
};

function toggle_color(select)
{
    let type = select.selectedOptions[0].label;
    let color = document.getElementById('color');

    if (type === 'TieFighter') {
        color.style.display = 'block';
    }
    else {
        color.style.display = 'none';
    }
}
