<?php
declare(strict_types = 1);

namespace App\Controller;

use \App\Model\BookingModel;
use \PommProject\Foundation\Where;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\DependencyInjection\ContainerAwareInterface;

final class BookingController implements ContainerAwareInterface
{
    use EntityController {
        saveAction as protected saveEntity;
    }

    public function getServiceName(): string
    {
        return 'controller.booking';
    }

    public function getEntityModel(): \PommProject\ModelManager\Model\Model
    {
        return $this->pomm['db']->getModel(BookingModel::class);
    }

    public function getDefaultEntity(): array
    {
        return [
            'id' => null,
            'client_id' => null,
            'vehicle_id' => null,
            'start_date' => '',
            'end_date' => '',
            'created_date' => '',
            'upgraded' => false,
        ];
    }

    public function getTemplateDir(): string
    {
        return 'bookings';
    }

    public function getDefaultSortKey(): string
    {
        return 'created_date DESC';
    }

    public function saveAction(Request $request, ?string $id): Response
    {
        $data = $request->request->get('entity');
        $data['upgraded'] = (isset($data['upgraded']) && $data['upgraded'] !== 'off');
        $request->request->set('entity', $data);

        return $this->saveEntity($request, $id);
    }

    public function listAction(Request $request): Response
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 20);

        $pager = $this->getEntityModel()
            ->paginateFindWhere(new Where, $limit, $page, 'ORDER BY ' . $this->getDefaultSortKey());

        $client_model = $this->pomm['db']->getModel(\App\Model\ClientModel::class);
        $pager->getIterator()->registerFilter(function($values) use($client_model) {
            $values['client'] = $client_model->findByPk(['id' => $values['client_id']]);
            return $values;
        });

        $vehicle_model = $this->pomm['db']->getModel(\App\Model\VehicleModel::class);
        $pager->getIterator()->registerFilter(function($values) use($vehicle_model) {
            $values['vehicle'] = $vehicle_model->findByPk(['id' => $values['vehicle_id']]);
            return $values;
        });

        return new Response(
            $this->templating->render(
                $this->getTemplateDir() . '/list.html.twig',
                compact('pager')
            )
        );
    }
}
