<?php
declare(strict_types = 1);

namespace App\Controller;

use \App\Model\ClientModel;
use \Symfony\Component\DependencyInjection\ContainerAwareInterface;

final class ClientController implements ContainerAwareInterface
{
    use EntityController;

    public function getServiceName(): string
    {
        return 'controller.client';
    }

    public function getEntityModel(): \PommProject\ModelManager\Model\Model
    {
        return $this->pomm['db']->getModel(ClientModel::class);
    }

    public function getDefaultEntity(): array
    {
        return [
            'id' => null,
            'firstname' => '',
            'lastname' => '',
        ];
    }

    public function getTemplateDir(): string
    {
        return 'clients';
    }

    public function getDefaultSortKey(): string
    {
        return 'lastname';
    }
}
