<?php
declare(strict_types = 1);

namespace App\Controller;

use \PommProject\Foundation\Pomm;
use \Symfony\Component\DependencyInjection\ContainerAwareInterface;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class ApiController implements ContainerAwareInterface
{
    use \Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

    private $pomm;

    public function __construct(Pomm $pomm)
    {
        $this->pomm = $pomm;
    }

    public function upgradableAction(Request $request, string $id): Response
    {
        if (!$request->request->has('vehicle_id')) {
            throw new BadRequestHttpException(
                'vehicle_id parameter is required'
            );
        }

        $vehicle_id = $request->request->get('vehicle_id');

        $sql = file_get_contents(__DIR__ . '/../sql/upgradable.sql');
        $sql = strtr($sql, [
            ":'client_id'" => "'$id'",
            ":'vehicle_id'" => "'$vehicle_id'",
        ]);
        $results = $this->pomm['db']->getQueryManager()
            ->query($sql)
            ->extract();

        return new JsonResponse($results[0]);
    }
}
