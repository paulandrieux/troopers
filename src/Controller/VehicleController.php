<?php
declare(strict_types = 1);

namespace App\Controller;

use \App\Model\VehicleModel;
use \Symfony\Component\DependencyInjection\ContainerAwareInterface;

final class VehicleController implements ContainerAwareInterface
{
    use EntityController;

    public function getServiceName(): string
    {
        return 'controller.vehicle';
    }

    public function getEntityModel(): \PommProject\ModelManager\Model\Model
    {
        return $this->pomm['db']->getModel(VehicleModel::class);
    }

    public function getDefaultEntity(): array
    {
        return [
            'id' => null,
            'name' => '',
            'type' => '',
            'color' => '',
        ];
    }

    public function getTemplateDir(): string
    {
        return 'vehicles';
    }

    public function getDefaultSortKey(): string
    {
        return 'name';
    }
}
