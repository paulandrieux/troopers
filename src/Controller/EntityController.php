<?php
declare(strict_types = 1);

namespace App\Controller;

use \PommProject\Foundation\Exception\SqlException;
use \PommProject\Foundation\Pomm;
use \PommProject\Foundation\Where;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use \Symfony\Component\Templating\EngineInterface;
use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

trait EntityController
{
    use \Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

    private $pomm;
    private $templating;

    public abstract function getServiceName(): string;
    public abstract function getEntityModel(): \PommProject\ModelManager\Model\Model;
    public abstract function getDefaultEntity(): array;
    public abstract function getDefaultSortKey(): string;
    public abstract function getTemplateDir(): string;

    public function __construct(EngineInterface $templating, Pomm $pomm)
    {
        $this->templating = $templating;
        $this->pomm = $pomm;
    }

    public function addAction(): Response
    {
        return $this->forward(
            $this->getServiceName() . ':editAction',
            ['id' => null]
        );
    }

    public function createAction(Request $request): Response
    {
        return $this->forward(
            $this->getServiceName() . ':saveAction',
            ['id' => null]
        );
    }

    public function editAction(?string $id): Response
    {
        $model = $this->getEntityModel();

        if ($id === null) {
            $entity = $model->createEntity($this->getDefaultEntity());
        } else {
            $entity = $model->findByPk(compact('id'));
            if ($entity === null) {
                throw new NotFoundHttpException("Unknow entity $id");
            }
        }

        $types = $this->getTypes();

        return new Response(
            $this->templating->render(
                $this->getTemplateDir() . '/edit.html.twig',
                [
                    'types' => $types,
                    'entity' => $entity,
                ]
            )
        );
    }

    public function saveAction(Request $request, ?string $id): Response
    {
        $data = $request->request->get('entity');
        foreach ($data as $key => $value) {
            if ($value === '') {
                unset($data[$key]);
            }
        }
        $model = $this->getEntityModel();

        try {
            if ($id === null) {
                $model->createAndSave($data);
                $this->addFlash('success', 'Entity created');
            } else {
                $pk = compact('id');
                $entity = $model->findByPk($pk);
                if ($entity === null) {
                    throw new NotFoundHttpException("Unknow entity $id");
                }
                $model->updateByPk($pk, $data);
                $this->addFlash('success', 'Entity updated');
            }
        } catch (SqlException $e) {
            $reason = 'Unknow error';
            if ($e->getSQLErrorState() === SqlException::CHECK_VIOLATION) {
                $reason = 'A X-Wing couldn’t have color';
            }

            $this->addFlash('error', "Unable to create entity: $reason");

            $types = $this->getTypes();
            $entity = ['id' => ''] + $data;

            return new Response(
                $this->templating->render(
                    $this->getTemplateDir() . '/edit.html.twig',
                    compact('types', 'entity')
                )
            );
        }

        return $this->redirect('/');
    }

    public function listAction(Request $request): Response
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 20);

        $pager = $this->getEntityModel()
            ->paginateFindWhere(new Where, $limit, $page, 'ORDER BY ' . $this->getDefaultSortKey());

        return new Response(
            $this->templating->render(
                $this->getTemplateDir() . '/list.html.twig',
                compact('pager')
            )
        );
    }

    public function deleteAction(string $id): Response
    {
        $model = $this->getEntityModel();
        $pk = compact('id');

        $entity = $model->findByPk($pk);
        if ($entity !== null) {
            $model->deleteByPk($pk);

            $this->addFlash('success', "Entity deleted");
        } else {
            throw new NotFoundHttpException("Unknow entity $id");
        }

        return $this->redirect('/');
    }

    private function getTypes(): array
    {
        $types = $this->pomm['db']->getQueryManager()
            ->query('SELECT enum_range(NULL::type)');

        return $types->get(0)['enum_range'];
    }
}
