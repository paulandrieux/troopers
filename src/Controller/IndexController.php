<?php
declare(strict_types = 1);

namespace App\Controller;

use \Symfony\Component\DependencyInjection\ContainerAwareInterface;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\RedirectResponse;

final class IndexController implements ContainerAwareInterface
{
    use \Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;

    public function indexAction(): Response
    {
        return new RedirectResponse('/bookings');
    }
}
