WITH count_vehicle AS (
    SELECT count(1)
        FROM vehicle v
),
quantity(type, count) AS (
    SELECT type, 100.0 * SUM(CASE WHEN v.type = type THEN 1 ELSE 0 END) / (SELECT count FROM count_vehicle)
        FROM vehicle v
        GROUP BY v.type
),
better_type(id, type) AS (
    SELECT id, CASE WHEN type = 'X-Wing' THEN 'TieFighter'::type ELSE type END
        FROM vehicle v
        WHERE v.id = id
),
dispo(type, count) AS (
    SELECT type, 100.0 - 100.0 * COUNT(b.vehicle_id) / (SELECT count FROM count_vehicle)
        FROM booking b
        JOIN vehicle v
            ON b.vehicle_id = v.id AND v.type = type
        WHERE b.start_date <= now()
            AND b.end_date >= now()
        GROUP BY v.type
),
recent_booking(client_id, count) AS (
    SELECT b.client_id, count(b.id)
        FROM booking b
        WHERE b.client_id = client_id
            AND b.created_date <= now() + '30 days'::interval
        GROUP BY b.client_id
)
SELECT q.count < 15.0 AND d.count > 50.0 AND r.count >= 2 AS upgradable
    FROM vehicle v
        LEFT JOIN quantity q
            ON q.type = v.type
        LEFT JOIN better_type b
            ON b.id = v.id
        LEFT JOIN dispo d
            ON d.type = b.type
        LEFT JOIN recent_booking r
            ON r.client_id = :'client_id'
    WHERE v.id = :'vehicle_id';
