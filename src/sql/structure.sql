CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'type') THEN
        CREATE TYPE type AS ENUM ('X-Wing', 'TieFighter');
    END IF;
END$$;

CREATE TABLE IF NOT EXISTS vehicle (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v1(),
    name TEXT NOT NULL,
    type type NOT NULL,
    color TEXT,

    CHECK(color IS NULL OR type = 'TieFighter')
);

CREATE TABLE IF NOT EXISTS client (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v1(),
    firstname TEXT NOT NULL,
    lastname TEXT
);

CREATE TABLE IF NOT EXISTS booking (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v1(),
    vehicle_id UUID NOT NULL,
    client_id UUID NOT NULL,
    upgraded BOOL NOT NULL,
    start_date TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
    end_date TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
    created_date TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,

    CHECK(start_date <= end_date),

    FOREIGN KEY (vehicle_id) REFERENCES vehicle(id),
    FOREIGN KEY (client_id) REFERENCES client(id)
);
