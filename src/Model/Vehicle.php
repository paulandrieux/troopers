<?php

namespace App\Model;

use PommProject\ModelManager\Model\FlexibleEntity;

/**
 * Vehicle
 *
 * Flexible entity for relation
 * public.vehicle
 *
 * @see FlexibleEntity
 */
class Vehicle extends FlexibleEntity
{
    public function __toString()
    {
        return $this->name;
    }
}
