<?php
declare(strict_types = 1);

namespace App\Model;

use \PommProject\Foundation\Session\Session;
use \PommProject\Foundation\Converter\PgString;
use \PommProject\ModelManager\SessionBuilder as BaseSessionBuilder;

class SessionBuilder extends BaseSessionBuilder
{
    protected function postConfigure(Session $session)
    {
        parent::postConfigure($session);

        $converter_holder = $session->getPoolerForType('converter')
            ->getConverterHolder();

        $converter_holder->registerConverter('Type', new PgString, ['type', 'public.type']);
    }
}
