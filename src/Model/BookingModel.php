<?php

namespace App\Model;

use PommProject\ModelManager\Model\Model;
use PommProject\ModelManager\Model\Projection;
use PommProject\ModelManager\Model\ModelTrait\WriteQueries;

use PommProject\Foundation\Where;

use App\Model\AutoStructure\Booking as BookingStructure;
use App\Model\Booking;

/**
 * BookingModel
 *
 * Model class for table booking.
 *
 * @see Model
 */
class BookingModel extends Model
{
    use WriteQueries;

    /**
     * __construct()
     *
     * Model constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->structure = new BookingStructure;
        $this->flexible_entity_class = '\App\Model\Booking';
    }
}
