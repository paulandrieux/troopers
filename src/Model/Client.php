<?php

namespace App\Model;

use PommProject\ModelManager\Model\FlexibleEntity;

/**
 * Client
 *
 * Flexible entity for relation
 * public.client
 *
 * @see FlexibleEntity
 */
class Client extends FlexibleEntity
{
    public function __toString()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
}
