<?php

namespace App\Model;

use PommProject\ModelManager\Model\FlexibleEntity;

/**
 * Booking
 *
 * Flexible entity for relation
 * public.booking
 *
 * @see FlexibleEntity
 */
class Booking extends FlexibleEntity
{
}
