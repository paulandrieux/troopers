<?php

namespace App\Model;

use PommProject\ModelManager\Model\Model;
use PommProject\ModelManager\Model\Projection;
use PommProject\ModelManager\Model\ModelTrait\WriteQueries;

use PommProject\Foundation\Where;

use App\Model\AutoStructure\Client as ClientStructure;
use App\Model\Client;

/**
 * ClientModel
 *
 * Model class for table client.
 *
 * @see Model
 */
class ClientModel extends Model
{
    use WriteQueries;

    /**
     * __construct()
     *
     * Model constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->structure = new ClientStructure;
        $this->flexible_entity_class = '\App\Model\Client';
    }
}
