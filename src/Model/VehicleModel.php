<?php

namespace App\Model;

use PommProject\ModelManager\Model\Model;
use PommProject\ModelManager\Model\Projection;
use PommProject\ModelManager\Model\ModelTrait\WriteQueries;

use PommProject\Foundation\Where;

use App\Model\AutoStructure\Vehicle as VehicleStructure;
use App\Model\Vehicle;

/**
 * VehicleModel
 *
 * Model class for table vehicle.
 *
 * @see Model
 */
class VehicleModel extends Model
{
    use WriteQueries;

    /**
     * __construct()
     *
     * Model constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->structure = new VehicleStructure;
        $this->flexible_entity_class = '\App\Model\Vehicle';
    }
}
