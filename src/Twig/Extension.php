<?php
declare(strict_types = 1);

namespace App\Twig;

final class Extension extends \Twig\Extension\AbstractExtension
{
    private $pomm;

    public function __construct(\PommProject\Foundation\Pomm $pomm)
    {
        $this->pomm = $pomm;
    }

    public function getFunctions()
    {
        return [
            new \Twig\TwigFunction('clients', [$this, 'clients']),
            new \Twig\TwigFunction('vehicles', [$this, 'vehicles']),
        ];
    }

    public function getTests()
    {
        return [
            new \Twig\TwigTest('instance_of', [$this, 'isInstanceOf']),
        ];
    }

    public function clients()
    {
        return $this->pomm['db']->getModel(\App\Model\ClientModel::class)
            ->findAll();
    }

    public function vehicles()
    {
        return $this->pomm['db']->getModel(\App\Model\VehicleModel::class)
            ->findAll();
    }

    public function isInstanceOf($var, $instance) {
        return  $var instanceof $instance;
    }
}
