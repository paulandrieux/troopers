Feature:

    Scenario Outline: Create a new clients
        When I am on "/clients/add"
        And I fill in "entity[firstname]" with "<firstname>"
        And I fill in "entity[lastname]" with "<lastname>"
        And I press "Create"
        Then I should see "Entity created"

        Examples:
            | firstname | lastname  |
            | Luke      | Skywalker |
            | Darth     | vador     |

    Scenario: list clients
        When I am on "/clients"
        Then I should see 3 "tr" elements

    Scenario: delete clients
        When I am on "/clients"
        And I follow "delete"
        Then I should see "Entity deleted"

    Scenario: edit clients
        When I am on "/clients"
        And I follow "edit"
        And I fill in "entity[lastname]" with "Maul"
        And I press "Save"
        Then I should see "Entity updated"

        When I am on "/clients"
        Then I should see "Maul"
