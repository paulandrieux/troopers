<?php
declare(strict_types = 1);

use \Behat\Behat\Context\Context;
use \Behat\Gherkin\Node\TableNode;

class FeatureContext implements Context
{
    private $pomm;
    private $clients;
    private $vehicles;
    private $bookings;

    public function __construct()
    {
        $this->clients = [];
        $this->vehicules = [];
        $this->bookings = [];
        $this->pomm = new \PommProject\Foundation\Pomm([
            'db' => [
                'dsn' => getenv('DATABASE_URL'),
                'class:session_builder' => '\App\Model\SessionBuilder',
            ],
        ]);
    }

    /**
     * @AfterScenario
     */
    public function cleanup(): void
    {
        $this->deleteEntities(\App\Model\BookingModel::class, $this->bookings);
        $this->deleteEntities(\App\Model\ClientModel::class, $this->clients);
        $this->deleteEntities(\App\Model\VehicleModel::class, $this->vehicules);
    }

    private function deleteEntities(string $model, array $entities): void
    {
        foreach($entities as $entity) {
            $this->pomm['db']->getModel($model)
                ->deleteByPk(['id' => $entity->getId()]);
        }
    }

    /**
     * @Given create a client with:
     */
    public function createClient(TableNode $entries): void
    {
        $this->clients += $this->createEntity(\App\Model\ClientModel::class, $entries);
    }

    /**
     * @Given create a vehicule with:
     */
    public function createVehicule(TableNode $entries): void
    {
        $this->vehicules += $this->createEntity(\App\Model\VehicleModel::class, $entries);
    }

    /**
     * @Given create a booking with:
     */
    public function createBooking(TableNode $entries): void
    {
        $this->bookings += $this->createEntity(\App\Model\BookingModel::class, $entries);
    }

    private function createEntity(string $model, TableNode $entries): array
    {
        $entities = [];
        foreach($entries as $entry) {
            $client = $this->pomm['db']->getModel($model)
                ->createAndSave($entry);
            $entities[] = $client;
        }

        return $entities;
    }
}
