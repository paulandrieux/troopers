Feature:
    Background:
        Given create a client with:
            | id                                   | firstname | lastname |
            | f55552dd-ee8d-4208-a5cd-3a3377593746 | Anakin    | Solo     |
        And create a vehicule with:
            | id                                   | type       | name           |
            | 9749d60a-8622-469d-af0c-7a407aaaa47a | X-Wing     | Harmony        |
            | 014344c4-c724-4bd8-8e14-d70f13eafc9c | TieFighter | Calypso        |
            | 459f3627-3c87-4b79-b2eb-f0f352612fe8 | TieFighter | Crash          |
            | 9d204666-4728-4973-a69d-0d2e0078abb3 | TieFighter | Actium         |
            | 2d0d03bc-bfa4-40db-a9c4-f80a8d35cca9 | TieFighter | Actium         |
            | 75618af8-ddd5-447b-8a2c-803c34c4482f | TieFighter | The Inquisitor |
            | d8d08d23-4935-4d10-adde-2312c80eab56 | TieFighter | Pursuit        |
        And create a booking with:
            | id                                   | client_id                            | vehicle_id                           | start_date | end_date   | upgraded |
            | 5a4d53dd-bd6b-4d8f-aef0-17dde88c0141 | f55552dd-ee8d-4208-a5cd-3a3377593746 | 014344c4-c724-4bd8-8e14-d70f13eafc9c | 2018-01-01 | 2099-01-01 | false    |
            | cfbc9b0a-de84-49bc-b369-66f5d6fc8459 | f55552dd-ee8d-4208-a5cd-3a3377593746 | 459f3627-3c87-4b79-b2eb-f0f352612fe8 | 2018-02-01 | 2018-02-01 | false    |

    Scenario:
        Given I add "Content-Type" header equal to "application/x-www-form-urlencoded"
        When I send a POST request to "/api/clients/f55552dd-ee8d-4208-a5cd-3a3377593746/upgradable" with parameters:
            | key        | value                                |
            | vehicle_id | 9749d60a-8622-469d-af0c-7a407aaaa47a |
        Then the response status code should be 200
        And the response should be in JSON
        And the JSON should be equal to:
        """
        {
            "upgradable": true
        }
        """
