Feature:

    Scenario: Create a new X-Wing
        When I am on "/vehicles/add"
        And I fill in "entity[name]" with "Interceptor"
        And I select "X-Wing" from "entity[type]"
        And I press "Create"
        Then I should see "Entity created"

    Scenario: Create a new X-Wing with color
        This should failed

        When I am on "/vehicles/add"
        And I fill in "entity[name]" with "Interceptor"
        And I select "X-Wing" from "entity[type]"
        And I fill in "entity[color]" with "red"
        And I press "Create"
        Then I should see "Unable to create entity: A X-Wing couldn’t have color"

    Scenario: Create a new TieFighter
        When I am on "/vehicles/add"
        And I fill in "entity[name]" with "Interceptor"
        And I select "TieFighter" from "entity[type]"
        And I press "Create"
        Then I should see "Entity created"

    Scenario: Create a new TieFighter with color
        When I am on "/vehicles/add"
        And I fill in "entity[name]" with "Interceptor"
        And I select "TieFighter" from "entity[type]"
        And I fill in "entity[color]" with "red"
        And I press "Create"
        Then I should see "Entity created"

    Scenario: list vehicles
        When I am on "/vehicles"
        Then I should see 4 "tr" elements

    Scenario: delete vehicle
        When I am on "/vehicles"
        And I follow "delete"
        Then I should see "Entity deleted"

        When I am on "/vehicles"
        And I should see 3 "tr" elements

    Scenario: edit vehicle
        When I am on "/vehicles"
        And I follow "edit"
        And I fill in "entity[name]" with "Interceptor 2"
        And I press "Save"
        Then I should see "Entity updated"

        When I am on "/vehicles"
        Then I should see "Interceptor 2"
