Feature:
    Background:
        Given create a client with:
            | id                                   | firstname | lastname |
            | f55552dd-ee8d-4208-a5cd-3a3377593746 | Anakin    | Solo     |
        And create a vehicule with:
            | id                                   | type       | name   | color |
            | 2d0d03bc-bfa4-40db-a9c4-f80a8d35cca9 | TieFighter | Actium | black |

    Scenario: Create a new booking
        When I am on "/bookings/add"
        And I select "Anakin Solo" from "entity[client_id]"
        And I select "Actium" from "entity[vehicle_id]"
        And I fill in "entity[start_date]" with "2018-01-01"
        And I fill in "entity[end_date]" with "2018-06-10"
        And I press "Create"
        Then I should see "Entity created"

    # Scenario: list bookings
        When I am on "/bookings"
        Then I should see 2 "tr" elements

    # Scenario: edit booking
        When I am on "/bookings"
        And I follow "edit"
        And I fill in "entity[end_date]" with "2019-01-01"
        And I press "Save"
        Then I should see "Entity updated"

    # Scenario: delete booking
        When I am on "/bookings"
        And I follow "delete"
        Then I should see "Entity deleted"
        And I am on "/bookings"
        And I should see "This is not entitity you are loocking for."
