# Troopers

[![Build Status](https://travis-ci.org/NicolasJoseph/troopers.svg?branch=master)](https://travis-ci.org/NicolasJoseph/troopers)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/NicolasJoseph/troopers/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/NicolasJoseph/troopers/?branch=master)

## Installation

```
$ git clone https://gitlab.com/sanpi/troopers.git
```

## Configuration

```
$ cp .env{.dist,}
$ make
```

## Run

```
$ APP_ENV=dev php -S localhost:8080 -t public/
```

## Test

```
$ bin/behat
```
